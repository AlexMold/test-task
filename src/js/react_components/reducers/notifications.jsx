import * as types from "../constants/actionTypes";
// import {omit, assign, mapValues} from 'lodash';
const initialState = [
    {
        id: 17,
        title: 'Test test test 17',
        unread: true,
        datetime: new Date()
    }, {
        id: 16,
        title: 'Test test test 16',
        unread: true,
        datetime: new Date().setHours((new Date()).getHours() - 2)
    }, {
        id: 14,
        title: 'Test test test 14',
        unread: true,
        datetime: new Date().setDate((new Date()).getDate() - 1)
    }, {
        id: 13,
        title: 'Test test test 13',
        unread: false,
        datetime: new Date().setDate((new Date()).getDate() - 3)
    }, {
        id: 12,
        title: 'Test test test 12',
        unread: false,
        datetime: new Date().setDate((new Date()).getDate() - 8)
    }, {
        id: 11,
        title: 'Test test test 11',
        unread: false,
        datetime: new Date().setDate((new Date()).getDate() - 31)
    }, {
        id: 10,
        title: 'Test test test 10',
        unread: false,
        datetime: new Date().setDate((new Date()).getDate() - 160)
    }
];

// const sortedState = initialState.sort((a, b) => a.id - b.id)

export default function notifications(state = initialState, action) {
    switch (action.type) {
        case types.ADD_TITLE:
            const sortedState = state.length ? state.sort((a, b) => a.id - b.id) : [{ id: 0 }];
            const newId = sortedState[sortedState.length - 1].id + 1;

            return state.concat({ 
                id: newId, title: action.title, unread: true, datetime: new Date() 
            });

        case types.ALL_READ:
            return state.map(item => {
                item.unread
                    ? item.unread = false
                    : '';
                return item;
            });

        case types.DELETE_ALL:
            return [];

        default:
            return state;
    }
}
