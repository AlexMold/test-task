# Alex Gribcov

[![N|AlexMold](https://avatars2.githubusercontent.com/u/7162245?v=3&s=100)](https://github.com/AlexMold/)

### Tech

Test Task uses a number of open source projects to work properly:

* [ReactJS] - HTML enhanced for web apps )
* [webpack]
* [redux]
* [react-redux]
* [yarn]
* [express]
* [semantic-ui-react]


### Installation

Dillinger requires [Node.js](https://nodejs.org/) v6+ to run.

Download and extract the [latest pre-built release](https://github.com/joemccann/dillinger/releases).

For production environments...

```sh
$ npm run build
$ npm run server
```

License
----

MIT

